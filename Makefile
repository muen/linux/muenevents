obj-m           := muenevents.o
muenevents-objs := main.o fs.o

KERNEL_SOURCE ?= /lib/modules/$(shell uname -r)/build

PWD := $(shell pwd)

all: compile-module

compile-module:
	$(MAKE) -C $(KERNEL_SOURCE) M=$(PWD) modules

install: install-module

install-module:
	install -d $(DESTDIR)/lib/modules/extra
	install -m 644 muenevents.ko $(DESTDIR)/lib/modules/extra

clean:
	$(MAKE) -C $(KERNEL_SOURCE) M=$(PWD) clean
