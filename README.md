# Muenevents

## Introduction

The *muenevents* Linux kernel module implements a virtual file system that
facilitates user-space access to source events (subject-specific hypercalls)
provided by the [Muen Separation Kernel][1].

For each source event a file with the corresponding logical name is shown in
the file system. A program can *write(2)* to the files to trigger an event.
Using *read(2)* to access the file returns the source event number.

## Usage

    $ modprobe muenevents
    $ mkdir -p /muenevents
    $ mount -t muenevents none /muenevents

## Authentication

The current authentication model is that the files are created with uid and gid
set to 0 and permissions 0600. No further capability checking is done by this
module.

[1]: https://muen.sk
