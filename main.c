// SPDX-License-Identifier: GPL-2.0+
/*
 * Muen source events file system.
 *
 * Copyright (C) 2020  secunet Security Networks AG
 * Copyright (C) 2020  codelabs GmbH
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file main.c
 * @brief Muenevents register/unregister functionality.
 *
 * These routines are dealing with file system initialization and cleanup.
 */

#include <linux/module.h>
#include <linux/fs.h>

#include "internal.h"

/**
 * @brief Module initialization.
 *
 * This function registers the Muen events file system.
 *
 * @return 0 on success, negative values on error
 */
static int __init muenevents_init(void)
{
	return register_filesystem(&muenevents_type);
}

/**
 * @brief Module finalization.
 *
 * This function unregisters the Muen events file system.
 */
static void __exit muenevents_exit(void)
{
	unregister_filesystem(&muenevents_type);
}

module_init(muenevents_init);
module_exit(muenevents_exit);

MODULE_DESCRIPTION("Muen SK source events file system");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Adrian-Ken Rueegsegger <ken@codelabs.ch>");
