// SPDX-License-Identifier: GPL-2.0+
/*
 * Muen source events file system.
 *
 * Copyright (C) 2020  secunet Security Networks AG
 * Copyright (C) 2020  codelabs GmbH
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file fs.c
 * @brief Implementation of the file system and the file operations.
 *
 * This kernel module implements a file system named "muenevents". It
 * facilitates user-space to trigger source events (subject-specific
 * hypercalls) provided by the Muen Separation Kernel.
 *
 * For each source event a file with the corresponding logical name is shown in
 * the file system. A program can write to the files to trigger an event.
 * Reading from a file returns the source event number.
 *
 * The current authentication model is that the files are created with uid and
 * gid set to 0 and permissions 0600. No further capability checking is done by
 * this module.
 */

#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/pagemap.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <muen/smp.h>

#include "internal.h"

/**
 * @brief Random magic value to identify this file system.
 */
#define MUENEVENTS_MAGIC 0x9a9bc92b

/**
 * @brief Size of temporary string buffer.
 */
#define TMPBUFLEN 20

/**
 * @brief Retrieve #source_event_t from file.
 *
 * This function retrieves the source event information previously stored with
 * set_event_info().
 *
 * @param file file pointer
 * @return the source_event_t stored in the private_data element.
 */
static inline struct source_event_t *get_event_info(struct file *file)
{
	return file->private_data;
}

/**
 * @brief Store #source_event_t into file.
 *
 * This function uses the file private_data element to store the source event
 * information.
 *
 * @param file file pointer
 * @param info source event information
 */
static inline void set_event_info(struct file *file,
				   struct source_event_t *info)
{
	file->private_data = info;
}

/**
 * @brief Open a file representing a source event.
 *
 * This function transfers the private data pointing to the
 * source event from the inode to the file.
 *
 * @param inode inode information
 * @param file file information
 * @return 0 on success
 */
static int muenevents_open(struct inode *inode, struct file *file)
{
	struct source_event_t *cur_event = inode->i_private;

	set_event_info(file, cur_event);
	return 0;
}

/**
 * @brief Read event number associated with given file.
 *
 * This function writes the source event number to the user area.
 *
 * @param file file pointer
 * @param buffer user-space buffer to store the event number
 * @param count maximum number of bytes to read
 * @param offset pointer to current position
 * @return number of bytes read
 * @return -EFAULT on error
 */
static ssize_t muenevents_read(struct file *file,
			   char __user *buffer,
			   size_t count,
			   loff_t *offset)
{
	const struct source_event_t * const cur_event = get_event_info(file);
	int len;
	char tmp[TMPBUFLEN];

	len = snprintf(tmp, TMPBUFLEN, "%lu\n", cur_event->number);
	if (len < 0)
		return -EFAULT;

	if (*offset > len)
		return 0;

	if (count > len - *offset)
		count = len - *offset;

	if (copy_to_user(buffer, tmp + *offset, count))
		return -EFAULT;

	*offset += count;
	return count;
}

/**
 * @brief Trigger source event identified by given file.
 *
 * This function triggers the source event associated with the file. Data
 * provided by the user is ignored.
 *
 * @param file file pointer
 * @param buffer user-space buffer, unused
 * @param count maximum number of bytes to write
 * @param offset pointer to current position, unused
 * @return 1 on success
 */
static ssize_t muenevents_write(struct file *file,
			    const char __user *buffer,
			    size_t count,
			    loff_t *offset)
{
	const struct source_event_t * const cur_event = get_event_info(file);

	muen_smp_trigger_event(cur_event->number, cur_event->cpu);
	return count;
}

/**
 * @brief File operations for this file system.
 */
static const struct file_operations muenevents_file_fops = {
	.open  = muenevents_open,
	.read  = muenevents_read,
	.write = muenevents_write,
};

/**
 * @brief Create a new inode.
 *
 * @param sb super block of our file system
 * @param mode the file mode to use (includes specification of the file type)
 * @return a new inode on success
 * @return NULL if inode could not be allocated
 */
static struct inode *muenevents_make_inode(struct super_block *sb, int mode)
{
	struct inode *ret = new_inode(sb);

	if (ret) {
		ret->i_mode = mode;
		ret->i_uid.val = ret->i_gid.val = 0;
		ret->i_blocks = 0;
		ret->i_atime = ret->i_mtime = ret->i_ctime = current_time(ret);
	}
	return ret;
}

/**
 * @brief Muen Sinfo filter function which only matches events.
 */
static bool muenevents_match_evts(const struct muen_cpu_affinity *const aff,
		void *match_data)
{
	return aff->res.kind == MUEN_RES_EVENT;
}

/**
 * @brief Create a file.
 *
 * This function creates a new file in our file system. The event given as
 * parameter is stored in the private data of the inode for later retrieval.
 * File mode is set to 0600 for read-write access. The file size is set to 1
 * byte.
 *
 * @param info CPU affinity info of the source event associated with the file
 * @param sb refers to the super block of the file system
 * @param dir refers to the parent directory
 * @return true on success
 * @return false if directory entry could not be allocated
 */
static bool muenevents_create_file(const struct muen_cpu_affinity *const info,
		struct super_block *sb, struct dentry *dir)
{
	const int file_mode = 0600;
	struct dentry *dentry;
	struct inode *inode;
	struct qstr qname;
	struct source_event_t *event;

	event = kzalloc(sizeof(*event), GFP_KERNEL);
	if (!event)
		goto out;

	strncpy(event->name, info->res.name.data, info->res.name.length);
	event->number = info->res.data.number;
	event->cpu = info->cpu;

	/* create hashed name */
	qname.name = event->name;
	qname.len = strlen(event->name);
	qname.hash = full_name_hash(dir, event->name, qname.len);

	inode = muenevents_make_inode(sb, S_IFREG | file_mode);
	if (!inode)
		goto out_free;

	inode->i_ino = get_next_ino();
	inode->i_size = 1;
	inode->i_fop = &muenevents_file_fops;
	inode->i_private = event;

	dentry = d_alloc(dir, &qname);
	if (!dentry)
		goto out_iput;

	/* put into cache and return */
	d_add(dentry, inode);
	pr_info("muenevents: registered file %s - event number %lu (CPU %u)",
	       event->name, event->number, event->cpu);
	return true;

out_iput:
	iput(inode);
out_free:
	kfree(event);
out:
	return false;
}

/**
 * @brief Create a file for each Muen source event
 *
 * This function creates a new file in the root directory for every Muen
 * event that is present.
 *
 * @param sb super block of the file system
 * @return 0 on success
 * @return -ENOMEN on error
 */
static int muenevents_create_files(struct super_block *sb)
{
	struct muen_cpu_affinity affinity;
	struct muen_cpu_affinity *entry;
	unsigned int affinity_count;
	int ret = 0;

	affinity_count = muen_smp_get_res_affinity(&affinity,
			&muenevents_match_evts, NULL);
	if (affinity_count < 1) {
		pr_info("muenevents: No Muen source events available\n");
		return 0;
	}

	list_for_each_entry(entry, &affinity.list, list)
		if (!muenevents_create_file(entry, sb, sb->s_root))
			ret = -ENOMEM;

	muen_smp_free_res_affinity(&affinity);

	return ret;
}

/**
 * @brief Fills the super block of the file system.
 *
 * This functions initializes the super block, creates the root directory and
 * the files using the muenevents_create_files() function.
 *
 * @param sb the super block of the file system
 * @param data file system options, currently ignored
 * @param silent don't display any printk message if true (ignored)
 * @return 0 on success
 * @see simple_fill_super() and muenevents_create_files() for possible error
 * conditions
 */
static int muenevents_fill_super(struct super_block *sb, void *data, int silent)
{
	static struct tree_descr empty_descr = {""};
	int err;

	err = simple_fill_super(sb, MUENEVENTS_MAGIC, &empty_descr);
	if (err)
		return err;

	return muenevents_create_files(sb);
	/* TODO: Register dentry_operations to free
	 * allocated source event structures.
	 */
}

/**
 * @brief Mount super block.
 *
 * This function uses mount_single() to provide a single instance of the file
 * system. The function muenevents_fill_super() is specified to fill the super
 * block of the instance.
 *
 * @param fst file system type specification
 * @param flags parameters specified in the user-space for this mount operation
 * @param devname device to mount, ignored
 * @param data file system options specified in user-space
 * @return pointer or error condition returned by mount_single()
 */
static struct dentry *muenevents_mount(struct file_system_type *fst,
				  int flags, const char *devname, void *data)
{
	return mount_single(fst, flags, data, muenevents_fill_super);
}

/**
 * This description contains the owner, the name, the operation to get the
 * super block, and the operation to destroy the super block. Here
 * kill_litter_super() is required as we are holding references to the
 * directory entries in the file system.
 */
struct file_system_type muenevents_type = {
	.owner		= THIS_MODULE,
	.name		= "muenevents",
	.mount		= muenevents_mount,
	.kill_sb	= kill_litter_super,
};
