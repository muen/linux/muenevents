/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Muen source events file system.
 *
 * Copyright (C) 2020  secunet Security Networks AG
 * Copyright (C) 2020  codelabs GmbH
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef INTERNAL_H_
#define INTERNAL_H_

/**
 * @file internal.h
 * @brief Common information shared by the other source files.
 */

#include <linux/types.h>
#include <linux/list.h>
#include <muen/sinfo.h>

/**
 * @brief Structure holding information about source events.
 *
 * This structure holds information about each source event. The information is
 * provided by the Muen SK in the subject info page.
 */
struct source_event_t {
	char name[MAX_NAME_LENGTH + 1]; /**< the name of the source event     */
	unsigned long number;           /**< the number of the source event   */
	unsigned int cpu;               /**< CPU affinity of the source event */
};

/**
 * @brief Description of the Muen events file system.
 */
extern struct file_system_type muenevents_type;

#endif
